#### Warm-up task (25 marks): Explain precisely what "not being thread safe" means in this context. Don't give general statements, talk about the Counter class specifically.
The idea of not being thread safe means that the class Counter has the inability to be executed or used using multiple threads.
In this case, the Counter class is a static class, meaning that there can only ever be one instance of it since the constructor is
private. Wih the implementation of Counter, without any changes, we can see this issue of not being thread safe as the game
of Sheep and Wolves does not run (or hanging). This means that the threads are getting stuck or are in such a state where they are waiting
on one another. This can be known as a deadlock, as each thread is most likely holding onto a resource of Counter and cannot
continue execution without the other resource that they cannot access. Therefore this results in a deadlock.

The deadlock in this situation comes from how the characters are trying to decrement the Counter object as the game is checking
to see that the Counter object has been depleted. Each of these threads are accessing the same resource in Counter. Since the 
thread in Main.java is constantly checking the value of Counter, it creates a lock on the object. At the same time the 
characters have the inability to decrement this counter. Therefore the Counter will never deplete and the characters will
never be able to move again since they require the Counter to be depleted before doing so.
