package main;

import java.util.Random;

import lib.Counter;

public class Demo implements Runnable{

	private static Thread threadZero, threadOne, threadTwo;
	private static int counterSize;

	public Demo() {
	}

	public static void main(String[] args) {
		
		threadZero = new Thread(new Demo());
		threadOne = new Thread(new Demo());
		threadTwo = new Thread(new Demo());
		counterSize = 10;
		Counter.set(counterSize);
		threadZero.start();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		counterSize = 5;
		Counter.set(counterSize);
		threadOne.start();
		counterSize = 2;
		Counter.set(counterSize);
		threadTwo.start();
	}

	@Override
	public void run() {
		for(int i = counterSize; i > 0; i--){
			System.out.println("Value of i: " + i);
			Counter.decrement();
			System.out.println("According to " + Thread.currentThread().getName() + " is the Counter depleted? " + Counter.depleted());
		}
	}
}
