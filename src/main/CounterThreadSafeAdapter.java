package main;

import lib.Counter;

public class CounterThreadSafeAdapter {


	private CounterThreadSafeAdapter() {
	}

	public static void setCounter(int a){
			Counter.set(a);
	}
	
	public static void decrementCounter(){
		synchronized(Counter.class){
			Counter.decrement();
		}
	}
	
	public static boolean isDepleted(){
			return Counter.depleted();
	}
}
