package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import behaviours.Chase;
import behaviours.Passive;
import onscreen.*;
import main.CounterThreadSafeAdapter;

public class Stage extends    javax.swing.JPanel
                   implements MouseListener,
                              MouseMotionListener {
	private static Stage instance = new Stage();

	public Grid grid;
	public onscreen.Character sheep;
	public List<onscreen.Character> sheepHerd;
	public onscreen.Character wolf;
	public onscreen.Character shepherd;
	public List<onscreen.Character> rabbitHerd;
	public boolean readyToStep(){
		return CounterThreadSafeAdapter.isDepleted();
	}
	public boolean sheepCaught = false;
	public boolean shepJump = false;
	

	Point lastMouseLoc = new Point(0, 0);

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		grid = new Grid();
		
		for(Cell c : grid) registerMouseObserver(c);
		
		shepherd = new Shepherd(grid.giveMeRandomCell(), new Passive());
		rabbitHerd = new ArrayList<onscreen.Character>(10);
		for(int i = 0; i < 10; i++){ 
			rabbitHerd.add(i, new RabbitAdaptor(grid.giveMeRandomCell()));
		}
		sheepHerd = new ArrayList<onscreen.Character>(5);
		for(int i = 0; i < 5; i++){
			sheepHerd.add(i, new Sheep(grid.giveMeRandomCell(), new Chase(shepherd)));
		}
		Cell wolfLoc = grid.giveMeRandomCell();
		wolf = new Wolf(wolfLoc, new Chase(closestSheep(wolfLoc)));

		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public onscreen.Character closestSheep(Cell myLocation){
		int smallestDistance = 20;
		int currDistance = 0;
		int closestSheep = 0;
		for(int i = 0; i < 5; i++){
			Cell sheepLoc = sheepHerd.get(i).getLocation();
			if((sheepLoc.x > 16 && sheepLoc.y > 16) || (sheepLoc.x < 4 && sheepLoc.y < 4)){
				i++;
			}
			currDistance = sheepLoc.distanceTo(myLocation);
			if(currDistance < smallestDistance){
				smallestDistance = currDistance;
				closestSheep = i;
			}
		}
		if(closestSheep >= 5){
			closestSheep = 4;
		}
		return sheepHerd.get(closestSheep);
	}

	public static Stage getInstance() {
		return instance;
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public void draw(Graphics g) {
		grid.draw(g);
		wolf.draw(g);
		shepherd.draw(g);
		for(onscreen.Character rabbit: rabbitHerd){rabbit.draw(g);}
		for(onscreen.Character sheep: sheepHerd){sheep.draw(g);}
		for(int i = 0; i<20; i++){
			for(int j = 0; j<20; j++){
				if((i+1)%5 == 0 && (j+1)%5 == 0){
					if(shepherd.getLocation().equals(grid.getCell(i, j))){
						shepJump = true;
					}
				}
			}
		}
		if (result() == -1){
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200,200);
		}
		else if (result() == 1) {
			g.setColor(Color.yellow);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("You Win!", 200, 200);
		}
	}

	public void step() {
		CounterThreadSafeAdapter.setCounter(1 + rabbitHerd.size() + sheepHerd.size());
		Cell wolfLoc = wolf.getLocation();
		wolf = new Wolf(wolfLoc, new Chase(closestSheep(wolfLoc)));
		new Thread(wolf).start();
		for(onscreen.Character sheep: sheepHerd){
			if((sheep.getLocation().x > 16 && sheep.getLocation().y > 16) || (sheep.getLocation().x < 4 && sheep.getLocation().y < 4)){
				sheep.setBehaviour(new Passive());
			}
			new Thread(sheep).start();
			if(!sheepCaught && sheep.getLocation() == shepherd.getLocation()) {
				shepherd = new SheepCarrier(shepherd);
			}
		}
		for(onscreen.Character rabbit: rabbitHerd){new Thread(rabbit).start();}
		
		for(int i = 0; i<20; i++){
			for(int j = 0; j<20; j++){
				if((i+1)%5 == 0 && (j+1)%5 == 0){
					if(shepherd.getLocation().equals(grid.getCell(i, j))){
						shepJump = true;
					}
				}
			}
		}
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		int newX = from.x + Integer.signum(xdiff);
		int newY = from.y + Integer.signum(ydiff);
		if (cellOccupied(newX, newY))
			return from; //i..e you can't move directly closer, so stay still.
		return grid.getCell(newX, newY);
	}

	public Cell getAdjacent(Cell cell, Direction direction) {
		int newX = cell.x + direction.dx;
		int newY = cell.y + direction.dy;
		if (cellOccupied(newX, newY))
			return cell; // i.e. the adjacent cell in that direction is this very cell (that's a modelling problem)
		try {
			return grid.getCell(newX, newY);
		} catch (ArrayIndexOutOfBoundsException e) {
			return cell; //if the adjacency is off the grid, just return the original cell.
		}
	}

	public boolean cellOccupied(int x, int y){
		for(onscreen.Character rabbit: rabbitHerd){
			if (rabbit.getLocation().x == x && rabbit.getLocation().y == y)
				return true;
		}
		return false;
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e){
		if (shepherd.getBounds().contains(e.getPoint())){
		  shepherd.mouseClicked(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if(bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

  public int result(){
	int safeSheepCount = 0;
  	if (shepherd.getLocation().equals(wolf.getLocation())){
  		return -1;
  	} 
  	for(onscreen.Character sheep: sheepHerd){
  		if (wolf.getLocation().equals(sheep.getLocation())){
  	  		return -1;
  	  	} 
  		if (((sheep.getLocation().x > 16 && sheep.getLocation().y > 16) || (sheep.getLocation().x < 4 && sheep.getLocation().y < 4))) {
  			safeSheepCount++;
  	  	}
  	}
  	if(safeSheepCount == sheepHerd.size()){
  		return 1;
  	}
  	return 0;
  }
}
