package behaviours;
import main.*;
import onscreen.*;

public class Chase implements Behaviour {
  onscreen.Character target;
  
  public Chase(onscreen.Character target){this.target = target;}

  public Cell execute(Cell location){

    try{
    	return Stage.getInstance().oneCellCloserTo(location, target.getLocation());
    }
    catch(NullPointerException e){
    	System.out.println("Null pointer");
    }
	return location;
  }

}