For the Pass part, you can see in in the Demo.java class that when it is executed that there are lines printed to the console.
The value of 'i' shows what the current state of Counter should be at. By looking at these lines you can see that the threads are
stating that the Counter is depleted at different times which is incorrect as the Counter class is static. Therefore this shows that
Counter is not thread safe.

For the Credit part, CounterThreadSafeAdapter was implemented to allow counter to be used in Sheep and Wolves. Decrementing the counter was 
synchronized as this was the part of the counter that was being accessed by multiple threads and initially what made Counter not thread safe.
Now the value of Counter is correct throughout the game so that all the characters can now move when they need to. Now the characters 
can move around the game as they need to.

For the (High) Distinction part I used the Counter class. A sheep herd was added to the game to create slightly more depth to the gameplay.
To assist with adding this to the game, the Counter had to be enlarged by the amount of sheep now. This was implemented by setting the
Counter size the same way that the rabbitHeard set the Counter size. 

Another feature added to the game was that the shepherd has the abilty to jump on a jump pad (indicated by the purple cells) which allows
the shepherd to double jump. This feature assists the player to herd the sheep into the pen by jumping over the wolf to lure the sheep away from
the wolf.

To win the game, the shepherd must lure all the sheep into either of the pens that are located in the top-left and the bottom-right of the grid.
Once the sheep are in the pen, their behaviour is changed to passive so that they no longer chase the shepherd. At the same time, the wolf will
know if the sheep is in the pen and know that these "safe" sheep cannot be touched. At this point the wolf will then chase the next closest sheep
that is not in a pen. The idea of having the sheep know that they are safe allows the player to leave sheep in the pen as they go and collect more
sheep.